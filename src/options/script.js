'use strict';

if (chrome != undefined) {
  var browser = chrome;
}

function chromeCallback(success, fail) {
  // when passed as a callback, this function will check for errors in chrome
  // and then call the success or fail callbacks provided
  return function(result) {
    if (chrome.runtime.lastError) {
      fail(chrome.runtime.lastError);
    } else {
      success(result);
    }
  }
}

function loadOptions() {
  function success(result) {
    console.log(result);
    document.getElementById('autoTrySecure').checked = result.autoTrySecure;
    document.getElementById('autoTrySecure').addEventListener('change', function() {
      var setData = {
        autoTrySecure: document.getElementById('autoTrySecure').checked
      };
      function success() {}
      function fail(error) {
        alert(browser.i18n.getMessage('error'));
        console.error(error);
      }
      if (chrome != undefined) {
        browser.storage.local.set(setData, chromeCallback(success, fail));
      } else {
        browser.storage.local.set(setData).then(success, fail);
      }
    });

    document.getElementById('permanentExceptions').innerHTML = '';
    if (result.permanentAllowed && result.permanentAllowed.length != 0) {
      document.getElementById('permanentExceptionsSubdomainNote').style.display = '';
      for (var exception of result.permanentAllowed) {
        var tr = document.createElement('tr');
        var domainTd = document.createElement('td');
        domainTd.innerText = exception;
        tr.append(domainTd);
        var removeTd = document.createElement('td');
        var removeBtn = document.createElement('button');
        removeBtn.className = 'browser-style';
        removeBtn.innerText = browser.i18n.getMessage('optionsPermanentExceptionRemove');
        removeBtn.addEventListener('click', function(e) {
          e.preventDefault();
          /*
            <table>
              [more rows]
              <tr>
                <td>[domain]</td>
                <tr>
                  <button>[remove button]</button> [this]
                </tr>
              </tr>
              [more rows]
            </table>
          */
          var domain = this.parentElement.parentElement.childNodes[0].innerText;
          var me = this; // to have a reference to the current "this" in the callbacks
          function success(result) {
            var exceptions = result.permanentAllowed;
            if (exceptions && exceptions.length) {
              var i = exceptions.indexOf(domain);
              exceptions.splice(i, 1);
            } else {
              exceptions = [];
            }
            var setData = {
              permanentAllowed:  exceptions
            };
            function success() {
              var domainRow = me.parentElement.parentElement;
              domainRow.parentElement.removeChild(domainRow);
            }
            function fail(error) {
              alert(browser.i18n.getMessage('error'));
              console.error(error);
            }
            if (chrome != undefined) {
              browser.storage.local.set(setData, chromeCallback(success, fail));
            } else {
              browser.storage.local.set(setData).then(success, fail);
            }
          }
          function fail(error) {
            alert(browser.i18n.getMessage('error'));
            console.error(error);
          }
          if (chrome != undefined) {
            browser.storage.local.get('permanentAllowed', chromeCallback(success, fail));
          } else {
            browser.storage.local.get('permanentAllowed').then(success, fail);
          }
          return false;
        });
        removeTd.appendChild(removeBtn);
        tr.append(removeTd);
        document.getElementById('permanentExceptions').appendChild(tr);
      }
    } else {
      var tr = document.createElement('tr');
      var td = document.createElement('td');
      td.innerText = browser.i18n.getMessage('optionsPermanentExceptionsNone');
      tr.appendChild(td);
      document.getElementById('permanentExceptions').appendChild(tr);
      document.getElementById('permanentExceptionsSubdomainNote').style.display = 'none';
    }
  }
  function fail(error) {
    alert(browser.i18n.getMessage('error'));
    console.error(error);
  }
  var getData = [
    'permanentAllowed',
    'autoTrySecure'
  ];
  if (chrome != undefined) {
    browser.storage.local.get(getData, chromeCallback(success, fail));
  } else {
    browser.storage.local.get(getData).then(success, fail);
  }
  document.getElementById('autoTrySecureLabel').innerText = browser.i18n.getMessage('optionsAutoTrySecure');
  document.getElementById('permanentExceptionsTitle').innerText = browser.i18n.getMessage('optionsPermanentExceptionsTitle');
  document.getElementById('permanentExceptionsSubdomainNote').innerText = browser.i18n.getMessage('optionsPermanentExceptionsSubdomainNote');
  document.title = browser.i18n.getMessage('optionsTitle', browser.i18n.getMessage('name'));
}

window.addEventListener('load', loadOptions);