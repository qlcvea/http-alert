'use strict';

var url = '';
var domain = '';
var hasWww = false; // this will be set to true if the "www." at the start of the domain name is removed

if (chrome != undefined) {
  var browser = chrome;
}

function chromeCallback(success, fail) {
  // when passed as a callback, this function will check for errors in chrome
  // and then call the success or fail callbacks provided
  return function(result) {
    if (chrome.runtime.lastError) {
      fail(chrome.runtime.lastError);
    } else {
      success(result);
    }
  }
}

function allowCallback(permanent) {
  return function() {
    // if the "domain" is an IP, don't tell the user that "subdomains" are going to be unlocked
    var confirmMessageString = permanent ? 'allowPermanent' : 'allowTemp';
    var domainParts = domain.split('.');
    if (domainParts.length == 4) {
      var i;
      for (i in domainParts) {
        var int = parseInt(domainParts[i], 10);
        if (isNaN(int) || !(int >= 0 && int < 256)) {
          confirmMessageString = permanent ? 'allowPermanentSubdomains' : 'allowTempSubdomains';
          break;
        }
      }
    } else {
      confirmMessageString = permanent ? 'allowPermanentSubdomains' : 'allowTempSubdomains';
    }
    if (confirm(browser.i18n.getMessage(confirmMessageString, domain))) {
      if (permanent) {
        function success(result) {
          var exceptions = result.permanentAllowed;
          if (exceptions && exceptions.length) {
            exceptions[exceptions.length] = domain;
          } else {
            exceptions = [ domain ];
          }
          var setData = {
            permanentAllowed:  exceptions
          }
          function success() {
            window.location.replace(url);
          }
          function fail(error) {
            alert(browser.i18n.getMessage('error'));
            console.error(error);
          }
          if (chrome != undefined) {
            browser.storage.local.set(setData, chromeCallback(success, fail));
          } else {
            browser.storage.local.set(setData).then(success, fail);
          }
        }
        function fail(error) {
          alert(browser.i18n.getMessage('error'));
          console.error(error);
        }
        if (chrome != undefined) {
          browser.storage.local.get('permanentAllowed', chromeCallback(success, fail));
        } else {
          browser.storage.local.get('permanentAllowed').then(success, fail);
        }
      } else {
        function sentCallback() {
          goUrl(url);
        }
        if (chrome != undefined) {
          browser.runtime.sendMessage({
            allowTemp: domain
          }, sentCallback);
        } else {
          browser.runtime.sendMessage({
            allowTemp: domain
          }).then(sentCallback);
        }
      }
    }
  }
}

function trySecure() {
  document.getElementById('trySecure').disabled = true;
  document.getElementById('trySecure').innerText = browser.i18n.getMessage('alertTrySecureInProgress');
  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'https://' + (hasWww ? 'www.' : '') + domain + '/', true);
  xhr.onreadystatechange = function() {
    console.log(this);
    if(this.readyState == this.DONE) {
      if (this.status > 0) {
        // the request was sent succesfully, therefore the site supports HTTPS
        var secureUrl = url;
        secureUrl = url.substr(url.indexOf(':')); // remove the starting "http"
        secureUrl = 'https' + secureUrl;
        goUrl(secureUrl);
      } else {
        // an error occurred while sending the request. the site probably doesn't support HTTPS
        document.getElementById('trySecure').innerText = browser.i18n.getMessage('errorSecureUnsupported');
      }
    }
  };
  xhr.send();
}

function goUrl(url) {
  for (var button of document.getElementsByTagName('button')) {
    button.disabled = true;
  }
  window.location.replace(url);
}

window.addEventListener('load', function() {
  var query = window.location.search;
  if (query.startsWith('?')) {
    query = query.substr(1);
  }
  var queries = query.split('&');
  var str;
  for (str of queries) {
    var parameter = str.split('=');
    if (decodeURIComponent(parameter[0]) == 'url') {
      url = decodeURIComponent(parameter[1]);
      break;
    }
  }
  domain = url.split('/')[2];
  if (domain.startsWith('www.')) {
    hasWww = true;
    domain = domain.substr(4);
  }
  document.getElementById('description').innerText = browser.i18n.getMessage('alertText', domain);

  document.getElementById('trySecure').addEventListener('click', trySecure);
  document.getElementById('go').addEventListener('click', allowCallback(false));
  document.getElementById('goPermanent').addEventListener('click', allowCallback(true));

  if (history.length == 1) {
    document.getElementById('cancel').disabled = true;
  } else {
    document.getElementById('cancel').addEventListener('click', function() {
      history.go(-1);
    });
  }

  document.getElementById('question').innerText = browser.i18n.getMessage('alertQuestion');
  document.getElementById('trySecure').innerText = browser.i18n.getMessage('alertTrySecure');
  document.getElementById('go').innerText = browser.i18n.getMessage('alertGo');
  document.getElementById('goPermanent').innerText = browser.i18n.getMessage('alertGoPermanent');
  document.getElementById('cancel').innerText = browser.i18n.getMessage('alertCancel');
  document.title = browser.i18n.getMessage('alertTitle');

  document.getElementsByClassName('alert')[0].style.display = '';

  function success(result) {
    if (result.autoTrySecure) {
      trySecure();
    }
  }
  function fail(error) {
    alert(browser.i18n.getMessage('error'));
    console.error(error);
  }
  var getData = [
    'autoTrySecure'
  ];
  if (chrome != undefined) {
    browser.storage.local.get(getData, chromeCallback(success, fail));
  } else {
    browser.storage.local.get(getData).then(success, fail);
  }
});