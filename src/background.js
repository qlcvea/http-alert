'use strict';

function chromeCallback(success, fail) {
  // when passed as a callback, this function will check for errors in chrome
  // and then call the success or fail callbacks provided
  return function(result) {
    if (chrome.runtime.lastError) {
      fail(chrome.runtime.lastError);
    } else {
      success(result);
    }
  }
}

var tempAllowed = [];
var permanentAllowed = [];

var onHeadersReceivedFilter = {
  urls: [
    'http://*/*'
  ]
};

var onHeadersReceivedExtraInfoSpec = [
  'blocking',
  'responseHeaders'
];

function onStorageChanged(changes, areaName) {
  if (areaName == 'local') {
    if (changes.permanentAllowed && changes.permanentAllowed.newValue) {
      permanentAllowed = changes.permanentAllowed.newValue;
    }
  }
}

function onMessage(message) {
  if (message.allowTemp) {
    tempAllowed[tempAllowed.length] = message.allowTemp;
  }
}

function onHeadersReceived(details) {
  var response = {};
  if (details.statusCode >= 300 && details.statusCode < 400) {
    var headers = details.responseHeaders;
    var header;
    for (header of headers) {
      if (header.name.toLowerCase() == 'location') {
        if (header.value) {
          if (header.value.toLowerCase().startsWith('https://')) {
            return response;
          }
        }
        break;
      }
    }
  }
  if (details.url.startsWith('http://')) {
    var domain = details.url.split('/')[2];
    while (domain.length != 0) {
      if (tempAllowed.indexOf(domain) !== -1) {
        return response;
      }
      if (permanentAllowed.indexOf(domain) !== -1) {
        return response;
      }
      var domainParts = domain.split('.');
      // if the "domain" is an IP, then don't check "subdomains"
      if (domainParts.length == 4) {
        var isIp = true;
        var i;
        for (i in domainParts) {
          var int = parseInt(domainParts[i], 10);
          if (isNaN(int) || !(int >= 0 && int < 256)) {
            isIp = false;
            break;
          }
        }
        if (isIp) {
          break;
        }
      }
      domainParts.shift();
      domain = domainParts.join('.');
    }
    response.redirectUrl = browser.runtime.getURL('/block/index.html?url=' + encodeURIComponent(details.url));
  }
  return response;
}

function storageGetError(error) {
  console.error(error);
}

function storageFirstReadPermanentAllowedFinished(result) {
  if (result.permanentAllowed && result.permanentAllowed.length) {
    permanentAllowed = result.permanentAllowed;
  }
}

if (chrome != undefined) {
  var browser = chrome;

  browser.storage.local.get('permanentAllowed', chromeCallback(storageFirstReadPermanentAllowedFinished, storageGetError));
} else {
  browser.storage.local.get('permanentAllowed').then(storageFirstReadPermanentAllowedFinished, storageGetError);
}

browser.storage.onChanged.addListener(onStorageChanged);

browser.runtime.onMessage.addListener(onMessage);

browser.webRequest.onHeadersReceived.addListener(
  onHeadersReceived,
  onHeadersReceivedFilter,
  onHeadersReceivedExtraInfoSpec
);