# HTTP alert

An extension to add an interstitial alert before visiting HTTP (insecure) sites.

## Downloads

- [addons.mozilla.org](https://addons.mozilla.org/firefox/addon/http-alert?src=external-gitlab)
- [AMO Android](https://addons.mozilla.org/android/addon/http-alert?src=external-gitlab)
- [Chrome Web Store](https://chrome.google.com/webstore/detail/http-alert/bfikdbfkbgobbknobngaimbellkckpde?utm_source=gitlab&utm_medium=readme&utm_campaign=readme)
- [Microsoft Edge Addons](https://microsoftedge.microsoft.com/addons/detail/ahcifeciecjkiognkcfimdnhaijopjjn)